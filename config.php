<?php
define('HEADER', 'bilder.gildseth.com');
define('THEME', 'default');

define('BASEPATH', getcwd());
define('STORAGEDIR', BASEPATH.'/storage');
define('CACHEDIR', BASEPATH.'/cache');
define('DIRECTORYCACHE', CACHEDIR.'/dircache');

define('FATAL', true);
?>
