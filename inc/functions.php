<?php

function buildDirCache( $dir, $fp = null ) {
  $dirContent = glob($dir);
  if (is_null($fp)) {
    canWrite(DIRECTORYCACHE, FATAL);
    $fp = fopen(DIRECTORYCACHE, 'w');
  }
  foreach($dirContent as $dirEntry) {
    fwrite($fp, str_replace(STORAGEDIR, '', $dirEntry."\n"));
    if (is_dir($dirEntry)) {
      buildDirCache($dirEntry.'/*', $fp);
    }
  }
}

function canWrite( $file, $die = false ) {
  $result = touch($file);
  if (!$result && $die) {
    die('Unable to write to '.$file);
  }
  return $result;
}

function mkPath($hash) {
  if (!is_dir(CACHEDIR.'/'.$hash[0].$hash[1])) mkdir(CACHEDIR.'/'.$hash[0].$hash[1]);
  if (!is_dir(CACHEDIR.'/'.$hash[0].$hash[1].'/'.$hash[2].$hash[3])) mkdir(CACHEDIR.'/'.$hash[0].$hash[1].'/'.$hash[2].$hash[3]);
  if (!is_dir(CACHEDIR.'/'.$hash[0].$hash[1].'/'.$hash[2].$hash[3].'/'.$hash[4].$hash[5])) mkdir(CACHEDIR.'/'.$hash[0].$hash[1].'/'.$hash[2].$hash[3].'/'.$hash[4].$hash[5]);
}

function imageResize($maxWidth, $maxHeight, $fileFrom, $fileTo) {
    $img = false;
    if (is_file($fileFrom)) {
        $img = imagecreatefromstring(file_get_contents($fileFrom));
    }

    if($img) {
        $width = ImageSX($img);
        $height = ImageSY($img);
        $maxWidth = ($maxHeight=='*'?1000000:$maxHeight);
        $maxHeight = ($maxHeight=='*'?1000000:$maxHeight);

        if ($width > $maxWidth || $height > $maxHeight) {
            $ratioX = $width/$maxWidth;
            $ratioY = $height/$maxHeight;
            $ratio = ($ratioX > $ratioY)?$ratioX:$ratioY;

            $newWidth = $width / $ratio;
            $newHeight = $height / $ratio;
            $newImage=ImageCreateTrueColor($newWidth, $newHeight);
            ImageCopyResampled ($newImage,
                                $img, 0, 0, 0, 0,
                                $newWidth,
                                $newHeight,
                                $width,
                                $height);
            ImageJPEG($newImage, $fileTo, 80);
        } else {
            ImageJPEG($img, $fileTo, 80);
        }
        return true;
    } else {
        return false;
    }
}

?>
