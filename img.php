<?php
require_once 'config.php';
require_once 'inc/functions.php';
$sizes = array('s' => 128, 'm' => '1024', 'l' => 16384);
if (isset($_GET['s']) && isset($sizes[$_GET['s']])) {
  $size = $sizes[$_GET['s']];
} else {
  $size = $sizes['s'];
  $_GET['s'] = 's';
}

$validFiles = file(DIRECTORYCACHE, FILE_IGNORE_NEW_LINES);
if (isset($_GET['f']) && in_array($_GET['f'], $validFiles)) {
  $file = STORAGEDIR.$_GET['f'];
  $hash = md5($file);
  $cachedImgPath = CACHEDIR.'/'.$hash[0].$hash[1].'/'.$hash[2].$hash[3].'/'.$hash[4].$hash[5].'/'.$_GET['s'].'_'.$hash;
  if (!is_file($cachedImgPath)) {
    mkpath($hash);
    imageResize($size, $size, $file, $cachedImgPath);
  }
  header('Content-Type: '.image_type_to_mime_type(exif_imagetype($cachedImgPath)));
  readfile($cachedImgPath);
} else {
  header('Content-Type: image/png');
  readfile('graphics/file_broken.png');
  exit;
}



?>
