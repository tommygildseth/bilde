<div id="neigborThumb">
  <ul>
  <?php
  foreach ($dirs as $dir) {
    $dirname = basename($dir);
    echo '
      <li>
        <a href="?d='.$dir.'">
          <img src="graphics/folder.png" height="128" width="128" alt="Subfolder"/>
          <span>'.$dirname.'</span>
        </a>
      </li>
    ';
  }
  if (!empty($files)) {
    foreach ($files as $file) {
      $filename = basename($file);
      echo '
        <li>
          <a href="?f='.$file.'">
            <img src="img.php?f='.$file.'&amp;s=t" height="128" width="128" alt="" />
            <span>'.$filename.'</span>
          </a>
        </li>
      ';
    }
  }
  ?>
  </ul>





</div>
<div id="imageLarge">
  <a href="img/l/<?php echo htmlspecialchars($_GET['f']); ?>">
    <img src="img/m/<?php echo htmlspecialchars($_GET['f']); ?>" alt="" />
  </a>
</div>
<div id="imageMeta">

</div>
