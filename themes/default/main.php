<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
  <head>
    <title><?php echo htmlspecialchars(basename($_GET['d'])).' – '.HEADER;?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" type="text/css" media="all" href="/style/master.css" />
  </head>
  <body>
    <div id="body">
      <div id="header">
        <h1><div id="subheading">- <?php echo str_replace('_', ' ', htmlspecialchars(basename($_GET['d'])));?></div><span><?php echo HEADER;?></span></h1>

      </div>
      <div id="breadcrumb">
        <ul>
          <li><a href="/">Top</a></li>
        <?php
          $path = explode('/', $_GET['d']);
          array_shift($path);
          $pathComplete = '';
          foreach($path as $folder) {
            $pathComplete .= '/'.htmlspecialchars($folder);
            echo '<li> <span>»</span> <a href="?d='.$pathComplete.'">'.htmlspecialchars(str_replace('_', ' ', $folder)).'</a></li>';
          }
        ?>
        </ul>
      </div>
      <div id="content">
        <?php

        if (isset($render)) {
          include BASEPATH.'/themes/'.THEME.'/'.$render;
        } else {
          include BASEPATH.'/themes/'.THEME.'/content.php';
        }

        ?>
      </div>
      <div id="footer">abcd</div>
    </div>
  </body>
</html>
