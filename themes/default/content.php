<?php if (!empty($dirs)) { ?>
  <ul id="folderList">
    <?php
    foreach ($dirs as $dir) {
      $dirname = basename($dir);
      echo '
        <li>
          <a href="?d='.$dir.'">
            <img src="graphics/folder.png" height="128" width="128" alt="Subfolder"/>
            <span>'.$dirname.'</span>
          </a>
        </li>
      ';
    }
    ?>
  </ul>
<?php } ?>

<?php if (!empty($files)) { ?>
  <ul id="imageList">
    <?php
    foreach ($files as $file) {
      $filename = basename($file);
      echo '
        <li>
          <a href="?f='.$file.'">
            <img src="img.php?f='.$file.'&amp;s=t" height="128" width="128" alt="" />
            <span>'.$filename.'</span>
          </a>
        </li>
      ';
    }
    ?>
  </ul>
<?php } ?>
