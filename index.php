<?php
require_once 'config.php';
require_once 'inc/functions.php';

$headers = getallheaders();
if (isset($headers['Cache-Control'])) { unlink(DIRECTORYCACHE); }

if (!is_file(DIRECTORYCACHE)) {
  buildDirCache(STORAGEDIR.'/*');
}

$validDirs = file(DIRECTORYCACHE, FILE_IGNORE_NEW_LINES);

$cwd = STORAGEDIR.'/*';
if (isset($_GET['f']) && !in_array($_GET['f'], $validDirs)) {
  unset($_GET['f']);
} else if (isset($_GET['f'])) {
  $_GET['d'] = dirname($_GET['f']);
}

if (isset($_GET['d']) && in_array($_GET['d'], $validDirs)) {
  $cwd = STORAGEDIR.$_GET['d'].'/*';
}
$dirContent = glob($cwd);
$files      = array();
$dirs       = array();

foreach($dirContent as $entry) {
  $entry2 = str_replace(STORAGEDIR, '', $entry);
  if (is_dir($entry)) $dirs[] = $entry2;
  if (exif_imagetype($entry)) $files[] = $entry2;
}

if (isset($_GET['f'])) {
  $render = 'image_content.php';
  $exifData = exif_read_data(STORAGEDIR.$_GET['f']);
  //unset($exifData['MakerNote']);
  //print_r($exifData); exit;
}

require_once BASEPATH.'/themes/'.THEME.'/main.php';
?>
